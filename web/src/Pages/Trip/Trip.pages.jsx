import React from "react";
import { Grid } from "@material-ui/core";

import TripList from './tripList'

class Trip extends React.Component {
  render() {
    return (
      <div style={{marginLeft: 250, marginRight: 250}}>
      <Grid container direction="column" justify="center" alignItems="center">
        <Grid item xs={12}>
          <h1 className="textHeader">เที่ยวไหนดี</h1>
        </Grid>

        <Grid item xs={12}>
          <TripList />
        </Grid>

      </Grid>
      </div>
    );
  }
}

export default Trip;
