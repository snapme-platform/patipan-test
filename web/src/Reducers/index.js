import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import { tripReducers } from './tripReducers'

const reducer = combineReducers({
    tripReducers,

    form: formReducer
})

export default reducer;