const intitialState = {
    tripList: [],
  };
  
  export function tripReducers(state = intitialState, action) {
    switch (action.type) {
      case "GET_TRIP_SUCCESS":
        return {
          ...state,
          tripList: action.data,
        };

        case "GET_TRIP_FAILED":
        return {
          ...state,
          tripList: []
        };
  
      default:
        return state;
    }
  }
  